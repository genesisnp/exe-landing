(() => {
    const BTN_ENVIAR = document.getElementById('btn-enviar')

    const INPUTS = {
        name : document.getElementById("name"),
        lastname: document.getElementById("lastname"),
        phone: document.getElementById("phone"),
        email: document.getElementById("email"),
        message: document.getElementById("message"),
        checkbox: document.getElementById("check-authorize")
    }
    const isValidEmail = (email) => {
        let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
        regex.test(INPUTS.email.value) ? INPUTS.email.classList.add('is-valid') : INPUTS.email.classList.add('is-invalid')
    }
    const isValidPhone = (phone) => {
        let number = Number(phone)
        !Number.isNaN(number) && number !== 0 && number.toString().length == 9 ? INPUTS.phone.classList.add('is-valid') : INPUTS.phone.classList.add('is-invalid')
    }
    const validateGeneral = (name, lastname, message) => {
        name !== ''  ? INPUTS.name.classList.add('is-valid') : INPUTS.name.classList.add('is-invalid')
        lastname !== ''  ? INPUTS.lastname.classList.add('is-valid') : INPUTS.lastname.classList.add('is-invalid')
        message !== ''  ? INPUTS.message.classList.add('is-valid') : INPUTS.message.classList.add('is-invalid')
    }
    const resetErrorsForm = () => {
        let errors = document.querySelectorAll('.is-invalid')
        let success = document.querySelectorAll('.is-valid')

        errors.forEach(el => { el.classList.remove('is-invalid') })
        success.forEach(el => { el.classList.remove('is-valid') })
    }
    const validator = (resetButtonText) =>{
        validateGeneral(INPUTS.name.value, INPUTS.lastname.value, INPUTS.message.value)
        isValidEmail(INPUTS.email.value)
        isValidPhone( INPUTS.phone.value)
        resetButtonText()
    }

    BTN_ENVIAR.addEventListener('click', function(e) {
        e.preventDefault()
        resetErrorsForm()
        this.innerHTML = `<i class="fa fa-spinner fa-pulse fa-fw"></i>`
        //simulando tiempo de espera
        setTimeout(() => {
            validator(() => {
                this.innerHTML = "Enviar"
            })
        }, 1500)
    })
})()